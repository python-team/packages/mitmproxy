mitmproxy (8.1.1-2) unstable; urgency=medium

  * Team upload.
  * Add upstream patch to fix Python 3.11 compatibility (Closes: #1031787)

 -- Aron Xu <aron@debian.org>  Fri, 03 Mar 2023 01:21:00 +0800

mitmproxy (8.1.1-1) unstable; urgency=high

  * Team upload
  * New upstream version 8.1.1 (Closes: #1010938, CVE-2021-39214, CVE-2022-24766)
  * d/watch: Remove opts without prepended opts
  * Depend on python3-zstandard

  [ Debian Janitor ]
  * Update watch file format version to 4
  * Remove 2 obsolete maintscript entries in 1 files
  * Bump debhelper from old 12 to 13
  * Set upstream metadata fields: Repository-Browse
  * Update standards version to 4.6.2, no changes needed

  [ Agathe Porte ]
  * d/docs: README.rst -> README.md
  * d/clean: remove .tox
  * rebase patches

 -- Bastian Germann <bage@debian.org>  Sun, 05 Feb 2023 17:00:42 +0100

mitmproxy (6.0.2-2) unstable; urgency=medium

  * Team upload.

  [ Sandro Tosi ]
  * debian/watch
    - tweak search regex to optionally look for `v` before version

  [ Andrey Rakhmatullin ]
  * Remove references to the removed OpenSSL.SSL.{SSLv2,SSLv3}_METHOD (Closes:
    #1026525).
  * Update lintian overrides for the newer lintian.

 -- Andrey Rakhmatullin <wrar@debian.org>  Tue, 24 Jan 2023 23:18:17 +0400

mitmproxy (6.0.2-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches and downgrade dependencies.
  * Ignore test failures.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 06 Jan 2021 15:55:14 +0100

mitmproxy (5.3.0-3) unstable; urgency=high

  * Use "not" with -k, not "-" for pytest 6 compatibility
    (Closes: #977071).

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 04 Jan 2021 09:53:30 +0100

mitmproxy (5.3.0-2) unstable; urgency=medium

  * Pre-wrap dependencies of the binary package.
  * Fix the dependencies of the binary package (Closes: #977926, #977927)
  * Drop an explicit dependency on h11, wsproto depends on it already.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 30 Dec 2020 09:51:56 +0100

mitmproxy (5.3.0-1) unstable; urgency=medium

  * New upstream release (Closes: #976035, #975178).
    - Refresh patches
    - Bump build dependencies.

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 30 Nov 2020 12:10:21 +0100

mitmproxy (5.1.1-2) unstable; urgency=medium

  * Team upload.
  * Remove zstd dependency from setup.py as well (Closes: #963487).

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 29 Jun 2020 11:21:51 +0200

mitmproxy (5.1.1-1) unstable; urgency=medium

  * New upstream release (Closes: #963067, #959180).
    - Refresh patches.
    - Bump build dependencies.
    - Patch out zstd not yet available in Debian.
  * Maintain in a team.

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 19 Jun 2020 11:41:56 +0200

mitmproxy (4.0.4-6.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Debian Janitor ]
  * Update standards version, no changes needed.
  * Bump debhelper from old 9 to 12.

  [ Andrej Shadura ]
  * Apply an upstream patch for wsproto 0.13 compatibility (Closes: #948206)

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 08 Apr 2020 16:47:13 +0200

mitmproxy (4.0.4-6) unstable; urgency=medium

  * Fix symlinks to fontawesome (Closes: #928749)
  * Bump-up Standards-Version
  * Update lintian overrides

 -- Sebastien Delafond <seb@debian.org>  Thu, 23 May 2019 13:30:35 +0200

mitmproxy (4.0.4-5) unstable; urgency=medium

  * Add dependency on python3-pkg-resources (Closes: #923354)

 -- Sebastien Delafond <seb@debian.org>  Mon, 04 Mar 2019 10:49:20 +0100

mitmproxy (4.0.4-4) unstable; urgency=medium

  * Remove references to manpage in debian/rules
  * Blacklist tests that require internet access (Closes: #906976)

 -- Sebastien Delafond <seb@debian.org>  Thu, 30 Aug 2018 18:00:54 +0200

mitmproxy (4.0.4-3) unstable; urgency=medium

  * Remove manpages, as they're way outdated and upstream is not interested in maintaining them (Closes: #905588)

 -- Sebastien Delafond <seb@debian.org>  Mon, 06 Aug 2018 19:53:35 +0200

mitmproxy (4.0.4-2) unstable; urgency=medium

  * Add dependency on python3-h11 (Closes: #905587)

 -- Sebastien Delafond <seb@debian.org>  Mon, 06 Aug 2018 19:47:21 +0200

mitmproxy (4.0.4-1) unstable; urgency=medium

  * New upstream version 4.0.4
  * Remove CVE-2018-14505 patch

 -- Sebastien Delafond <seb@debian.org>  Sun, 05 Aug 2018 09:18:56 +0200

mitmproxy (3.0.4-1) unstable; urgency=medium

  * New upstream version 3.0.4
  * Rediff patches
  * Fix CVE-2018-14505 (Closes: #904293)

 -- Sebastien Delafond <seb@debian.org>  Sat, 04 Aug 2018 09:19:09 +0200

mitmproxy (3.0.3-1) unstable; urgency=medium

  * New upstream revision (Closes: #894436)

 -- Sebastien Delafond <seb@debian.org>  Mon, 02 Apr 2018 10:22:09 +0200

mitmproxy (2.0.2-4) unstable; urgency=medium

  * Rediff patches, and remove all upper-bounds on versioned conflicts
    following upstream recommendation in #846850 (Closes: #892500)

 -- Sebastien Delafond <seb@debian.org>  Mon, 19 Mar 2018 11:46:44 +0100

mitmproxy (2.0.2-3) unstable; urgency=medium

  * Switch to DEP-14

 -- Sebastien Delafond <seb@debian.org>  Tue, 06 Feb 2018 08:34:13 +0100

mitmproxy (2.0.2-2) unstable; urgency=medium

  * Update Vcs-* to point to salsa.d.o
  * Add lost debian/watch file
  * Remove unused lintian-overrides
  * Remove bundled font-awesome from binary package (Closes: #847914)

 -- Sebastien Delafond <seb@debian.org>  Wed, 24 Jan 2018 09:44:20 +0100

mitmproxy (2.0.2-1) unstable; urgency=medium

  * Imported Upstream version 2.0.2 (Closes: #869615, #867278, #867250,
    #849556, #849648)

 -- Sebastien Delafond <seb@debian.org>  Tue, 23 Jan 2018 08:54:28 +0100

mitmproxy (1.0.2-1) unstable; urgency=medium

  * New upstream version 1.0.2 (Closes: #849556, #852902)
  * Lintian fixes
  * Support python3 only
  * Use packaged version of Font Awesome in place of those excluded from
    upstream (Closes: #847914)
  * Remove unnecessary Build-Depend on cffi (Closes: #849648)
  * Correct Vcs-Git

 -- Sebastien Delafond <seb@debian.org>  Fri, 27 Jan 2017 16:55:32 +0100

mitmproxy (0.18.2-6) unstable; urgency=medium

  * Remove upper-bounds on all versioned dependencies (Closes: #848562)

 -- Sebastien Delafond <seb@debian.org>  Sun, 18 Dec 2016 13:43:09 +0100

mitmproxy (0.18.2-5) unstable; urgency=medium

  [ Carlos Maddela ]
  * Update copyright file to machine-readable format.
  * Show how we derive our upstream tarballs via watch and copyright files.
  * Update Standards-Version to 3.9.8.
  * Clean up mitmproxy.egg-info.
  * Use packaged version of Font Awesome in place of those excluded from upstream.
  * Prevent chrome-devtools LICENSE from being installed in undesired location.
  * Add more licensing details to copyright file.
  * Install examples via dh_installexamples instead of dh_installdocs.
  * Updated mitmproxy man page. (Closes: #806635, #812388)
  * Created man pages for remaining binaries.
  * Update Vcs-Git and Vcs-Browser to use secure protocols.
  * Override source-is-missing Lintian errors.

 -- Sebastien Delafond <seb@debian.org>  Mon, 12 Dec 2016 10:03:38 +0100

mitmproxy (0.18.2-4) unstable; urgency=medium

  * Relax python-passlib versioned dependencies, as the entire test suite
    passes with python-passlib 1.7.0-1 (Closes: #846850)

 -- Sébastien Delafond <sdelafond@gmail.com>  Sat, 10 Dec 2016 16:07:57 +0100

mitmproxy (0.18.2-3) unstable; urgency=medium

  * Add Homepage to debian/control (Closes: #844203)

 -- Sebastien Delafond <seb@debian.org>  Sun, 13 Nov 2016 14:25:45 +0100

mitmproxy (0.18.2-2) unstable; urgency=medium

  * Dependencies: 2016.9.19 for python-html2text is actually OK (Closes: #844060)

 -- Sebastien Delafond <seb@debian.org>  Sat, 12 Nov 2016 16:13:01 +0100

mitmproxy (0.18.2-1) unstable; urgency=medium

  * New upstream version 0.18.2 (Closes: #843549)

 -- Sebastien Delafond <seb@debian.org>  Tue, 08 Nov 2016 09:43:38 +0100

mitmproxy (0.18.1-2) unstable; urgency=medium

  * Add python-typing to list of dependencies (Closes: #842654)

 -- Sebastien Delafond <seb@debian.org>  Mon, 31 Oct 2016 10:41:19 +0100

mitmproxy (0.18.1-1) unstable; urgency=medium

  * New upstream version 0.18.1 (Closes: #836276)

 -- Sebastien Delafond <seb@debian.org>  Tue, 25 Oct 2016 09:49:36 +0200

mitmproxy (0.15-2) unstable; urgency=medium

  * Add CONTRIBUTORS and examples/ (Closes: #816189)

 -- Sebastien Delafond <seb@debian.org>  Thu, 03 Mar 2016 09:23:23 +0100

mitmproxy (0.15-1) unstable; urgency=medium

  * New upstream version 0.15

 -- Sebastien Delafond <seb@debian.org>  Wed, 10 Feb 2016 15:13:45 +0100

mitmproxy (0.13-1) unstable; urgency=medium

  * New upstream version (Closes: #800607)

 -- Sebastien Delafond <seb@debian.org>  Thu, 29 Oct 2015 15:07:15 +0100

mitmproxy (0.12.1-2) unstable; urgency=medium

  * Explicitly require python-netlib >= 0.12
  * Only recommend python-pyperclip instead of depending on it (Closes:
    #801279)

 -- Sebastien Delafond <seb@debian.org>  Fri, 09 Oct 2015 10:59:27 +0200

mitmproxy (0.12.1-1) unstable; urgency=medium

  * New upstream release (Closes: #789782)
  * Switch to dh

 -- Sebastien Delafond <seb@debian.org>  Thu, 01 Oct 2015 16:33:03 +0200

mitmproxy (0.11.3-3) unstable; urgency=medium

  * Versioned-depend on python-tornado >= 4.0.2 and python-netlib >=
    0.11.2 (Closes: #784303)

 -- Sebastien Delafond <seb@debian.org>  Mon, 22 Jun 2015 13:44:38 +0200

mitmproxy (0.11.3-2) unstable; urgency=medium

  * Depend on python-setuptools (Closes: #784932)

 -- Sebastien Delafond <seb@debian.org>  Tue, 19 May 2015 22:58:20 +0200

mitmproxy (0.11.3-1) unstable; urgency=medium

  * New upstream versio
  * Bump-up Standards-Version

 -- Sebastien Delafond <seb@debian.org>  Sat, 02 May 2015 13:44:26 +0200

mitmproxy (0.10.1-2) unstable; urgency=medium

  * Add a manpage for mitmproxy, graciously provided by Alex
    Chernyakhovsky (Closes: #731928)

 -- Sebastien Delafond <seb@debian.org>  Tue, 17 Jun 2014 10:59:06 +0200

mitmproxy (0.10.1-1) unstable; urgency=low

  * New upstream version
  * Bump up Standards-Version

 -- Sebastien Delafond <seb@debian.org>  Wed, 02 Apr 2014 13:19:31 +0200

mitmproxy (0.10-2) unstable; urgency=low

  * Record that lintian overrides were added for last upload (Closes: #737439)

 -- Sebastien Delafond <seb@debian.org>  Mon, 17 Feb 2014 09:52:36 +0100

mitmproxy (0.10-1) unstable; urgency=low

  * New upstream relase

 -- Sebastien Delafond <seb@debian.org>  Mon, 10 Feb 2014 14:07:27 +0100

mitmproxy (0.9.2-2) unstable; urgency=low

  * Change architecture to "all"

 -- Sebastien Delafond <seb@debian.org>  Tue, 31 Dec 2013 15:21:43 +0100

mitmproxy (0.9.2-1) unstable; urgency=low

  * New upstream release

 -- Sebastien Delafond <seb@debian.org>  Tue, 31 Dec 2013 12:41:31 +0100

mitmproxy (0.9.1-2) unstable; urgency=low

  * Add dependency on python-netlib (Closes: #724239)

 -- Sebastien Delafond <seb@debian.org>  Tue, 24 Sep 2013 09:28:30 +0200

mitmproxy (0.9.1-1) unstable; urgency=low

  * New upstream release (Closes: #717694)
  * Bump up Standards-Version
  * Update watch file

 -- Sebastien Delafond <seb@debian.org>  Sun, 01 Sep 2013 15:03:09 +0200

mitmproxy (0.8-2) unstable; urgency=low

  * Added missing dependency on python-lxml (Closes: #687056)

 -- Sebastien Delafond <seb@debian.org>  Mon, 10 Sep 2012 11:10:13 +0200

mitmproxy (0.8-1) unstable; urgency=low

  * Imported Upstream version 0.8
  * Bumped up Standards-Version
  * Added new upstream dependencies

 -- Sebastien Delafond <seb@debian.org>  Thu, 19 Apr 2012 11:24:19 +0200

mitmproxy (0.7-1) unstable; urgency=low

  * Imported Upstream version 0.7

 -- Sebastien Delafond <seb@debian.org>  Tue, 28 Feb 2012 15:51:20 +0100

mitmproxy (0.6-3) unstable; urgency=low

  * Section is network, not python (Closes: #660334)

 -- Sebastien Delafond <seb@debian.org>  Sat, 18 Feb 2012 13:44:08 +0100

mitmproxy (0.6-2) unstable; urgency=low

  * Add Vcs-* info to control file

 -- Sebastien Delafond <seb@debian.org>  Sat, 18 Feb 2012 10:04:08 +0100

mitmproxy (0.6-1) unstable; urgency=low

  * Initial release (Closes: #659613).

 -- Sebastien Delafond <seb@debian.org>  Mon, 13 Feb 2012 11:46:52 +0100
